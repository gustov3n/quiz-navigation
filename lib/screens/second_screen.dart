import 'package:flutter/material.dart';
import 'package:quiz_navigation/constants.dart';
import 'package:quiz_navigation/models/metode_perjalanan.dart';

class SecondScreen extends StatefulWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  State<SecondScreen> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  MetodePerjalanan? _selectedMetodePerjalanan;

  void _onMetodePerjalananChanged(metode) => setState(() {
        _selectedMetodePerjalanan = metode;
      });

  void _backToHome() => Navigator.pop(context, _selectedMetodePerjalanan?.icon);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(),
      body: Container(
        width: double.infinity,
        decoration: backgrounDecoration,
        child: Padding(
          padding: paddingAllMedium,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Pilih salah satu',
                textAlign: TextAlign.center,
                style: textStyleLargeBold.copyWith(color: Colors.white),
              ),
              verticalSpacer,
              Card(
                child: Padding(
                  padding: paddingAllMedium,
                  child: Column(children: [
                    ...allMetodePerjalanan
                        .map((metode) => RadioListTile(
                              value: metode,
                              groupValue: _selectedMetodePerjalanan,
                              onChanged: _onMetodePerjalananChanged,
                              title: Text(metode.name ?? ''),
                            ))
                        .toList(),
                    verticalSpacer,
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: _backToHome,
                        child: const Text('Udah'),
                      ),
                    ),
                  ]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
