import 'package:flutter/material.dart';
import 'package:quiz_navigation/constants.dart';
import 'package:quiz_navigation/screens/second_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  IconData? _iconMetodePerjalanan;

  void _goToSecondScreen() async {
    _iconMetodePerjalanan = await Navigator.push<IconData>(
        context, MaterialPageRoute(builder: (context) => const SecondScreen()));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: backgrounDecoration,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Halo ayank',
              textAlign: TextAlign.center,
              style: textStyleLargeBold.copyWith(color: Colors.white),
            ),
            Padding(
              padding: paddingAllMedium,
              child: Card(
                child: Padding(
                  padding: paddingAllLarge,
                  child: Column(
                    children: [
                      if (_iconMetodePerjalanan != null)
                        CircleAvatar(child: Icon(_iconMetodePerjalanan)),
                      verticalSpacer,
                      const Text('Metode perjalanan apakah yang anda sukai?'),
                      verticalSpacer,
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          onPressed: _goToSecondScreen,
                          child: const Text('Klik aku'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
