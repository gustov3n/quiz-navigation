import 'package:flutter/cupertino.dart';
import 'package:quiz_navigation/enums.dart';

class MetodePerjalanan {
  final TypeMetodePerjalanan type;
  final IconData? icon;
  final String? name;

  MetodePerjalanan({
    required this.type,
    this.icon,
    this.name,
  });
}
