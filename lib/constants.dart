import 'package:flutter/material.dart';

import 'enums.dart';
import 'models/metode_perjalanan.dart';

const BoxDecoration backgrounDecoration = BoxDecoration(
  gradient: LinearGradient(
    begin: Alignment.topLeft,
    colors: [
      Color(0xFFF6B2D5),
      Color(0xFF9B85EC),
    ],
  ),
);

const BorderRadius borderRadiusSmall = BorderRadius.all(Radius.circular(10));
const BorderRadius borderRadiusMedium = BorderRadius.all(Radius.circular(15));

const EdgeInsets paddingAllSmall = EdgeInsets.all(8);
const EdgeInsets paddingAllMedium = EdgeInsets.all(16);
const EdgeInsets paddingAllLarge = EdgeInsets.all(24);

const double _fontSizeMedium = 18;
const double _fontSizeLarge = 32;

const TextStyle textStyleMedium = TextStyle(fontSize: _fontSizeMedium);
const TextStyle textStyleLarge = TextStyle(fontSize: _fontSizeLarge);

const TextStyle textStyleBold = TextStyle(fontWeight: FontWeight.bold);
const TextStyle textStyleMediumBold =
    TextStyle(fontSize: _fontSizeMedium, fontWeight: FontWeight.bold);
const TextStyle textStyleLargeBold =
    TextStyle(fontSize: _fontSizeLarge, fontWeight: FontWeight.bold);

const Widget verticalSpacer = SizedBox(height: 16);

final allMetodePerjalanan = [
  MetodePerjalanan(
      type: TypeMetodePerjalanan.jalan,
      icon: Icons.directions_walk,
      name: "Jalan"),
  MetodePerjalanan(
      type: TypeMetodePerjalanan.sepeda,
      icon: Icons.motorcycle,
      name: "Sepeda"),
  MetodePerjalanan(
      type: TypeMetodePerjalanan.mobil, icon: Icons.drive_eta, name: "Mobil"),
  MetodePerjalanan(
      type: TypeMetodePerjalanan.pesawat, icon: Icons.flight, name: "Pesawat"),
];
